class UpdateEventSlots < ActiveRecord::Migration
  def change
    #Purge existing EventSlots
    EventSlot.destroy_all

    # Add start time column
    add_column :event_slots, :start_time, :datetime, null: false

    # Add team reference
    add_reference :event_slots, :team, null: false

    # Add product reference
    add_reference :event_slots, :product, null: false

    # Remove event reference
    remove_reference :event_slots, :event, index: true, foreign_key: true

    # Remove title and description
    remove_column :event_slots, :title, :text
    remove_column :event_slots, :description, :text
  end
end
