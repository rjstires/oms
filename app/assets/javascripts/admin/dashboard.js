Paloma.controller('Admin/Dashboard', {
    index: function () {
        $('#admin-dashboard-vacancies tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        });

        var table = $('#admin-dashboard-vacancies').DataTable({
            "order": [[0, 'asc'], [1, 'asc']],
            "pageLength": 10,
            "columnDefs": [
                {"orderable": false, "targets": 6}
            ]
        });

        table.columns().every(function () {
            var that = this;

            $('input', this.footer()).on('keyup change', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
    }
});