Paloma.controller('Teams', {
    dashboard: function () {
        $('.j-dataTable').DataTable({
            "order": [[0, 'asc']],
            "pageLength": 10,
            "columnDefs": [
                {"orderable": false, "targets": 3}
            ]
        })
    }
});