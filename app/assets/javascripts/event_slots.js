Paloma.controller('EventSlots', {
    new: function search() {
        var self = this;

        $('.product-option').on('change', updateProductSelect(self.params));
    },

    edit: function () {
        var self = this;

        $('.product-option').on('change', updateProductSelect(self.params));
    },

    index: function () {}
});

function updateProductSelect(params) {
    return function(e){
        var data = {};

        $('.product-option').each(function(i) {
            var $select = $(this);
            var $option = $select.find("option:selected:not([value=''])");
            var key = $select.attr('id');

            if($option.length > 0){
                data[key] = parseInt( $option.attr('value') );
            }
        });
        $('.product-selector__content').addClass('loading--div');
        $.ajax({
            url: params.products_search_url,
            data: {filters: data}
        })
            .done(function(results) {
                var $el = $("select[id*='_product_id']");
                $el.empty();
                $el.append($('<option>Select a Product</option>'));
                $.each(results.products, function(key, value) {
                    $el.append($('<option value="' + this.id + '">' + this.description + '</option>'));
                });
                $el.select2();
            })
            .fail(console.log)
            .always(function () {
                $('.product-selector__content').removeClass('loading--div');
            });
    };
}