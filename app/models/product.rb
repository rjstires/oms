class Product < ActiveRecord::Base
  belongs_to :category
  belongs_to :difficulty
  belongs_to :loot_option
  belongs_to :mount
  belongs_to :play_style
  belongs_to :zone

  validates_presence_of  :description, :category, :difficulty, :loot_option, :mount, :play_style, :zone

  before_save :downcase_fields

  scope :with_category, -> (v) { where(category: Category.name_is(v)) }
  scope :with_difficulty, -> (v) { where(difficulty: Difficulty.name_is(v)) }
  scope :with_loot_option, -> (v) { where(loot_option: LootOption.name_is(v)) }
  scope :with_mount, -> (v) { where(mount: Mount.name_is(v)) }
  scope :with_play_style, -> (v) { where(play_style: PlayStyle.name_is(v)) }
  scope :with_zone, -> (v) { where(zone: Zone.name_is(v)) }

  def downcase_fields
    self.description.downcase!
  end

  def display_name
     self.description.titleize
  end

  def to_label
    "#{self.description}"
  end

  def self.select_list
    includes(:category, :zone, :play_style, :loot_option, :difficulty, :mount).all
  end

  def self.search(params)
    scope = Product.includes(:category, :zone, :difficulty, :mount, :loot_option, :play_style).order(description: :asc)

    params[:filters].each do |filter|
      key = filter[0]
      value = filter[1]
      scope = scope.where(key.to_sym => value)
    end

    scope
  end
end
