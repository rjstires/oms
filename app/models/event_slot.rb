class EventSlot < ActiveRecord::Base
  belongs_to :team
  belongs_to :product
  belongs_to :character

  validates_presence_of :team, :product

  scope :upcoming, -> { where('event_slots.start_time > ?', Time.now) }
  scope :past, -> { where('event_slots.start_time < ?', Time.now) }
  scope :with_team, -> { includes(:team) }
  scope :with_product, -> { includes(:product) }
  scope :with_character, -> { includes(:character) }
  scope :vacant, -> { where(:character => nil) }

  def display_character
    if self.character
      self.character.display_name
    else
      'Vacant'
    end
  end
end
