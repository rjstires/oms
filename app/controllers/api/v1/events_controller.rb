module Api::V1
  class EventsController < ApiController
    skip_before_filter :authenticate_user!
    def index
      @events = Event.includes(
      :event_slots,
      :category,
      :zone,
      :difficulty,
      :team => [:faction],
      :character => [:classification])
      .upcoming_events
      .order(event_timestamp: :desc)
    end
  end
end
