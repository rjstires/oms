module Api::V1
  class EventSlotsController < ApiController
    skip_before_filter :authenticate_user!
    def index
      @event_slots = EventSlot.upcoming
                         .with_character
                         .with_team
                         .with_product
                         .order(start_time: :asc)
    end
  end
end
