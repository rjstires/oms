class EventSlotsController < ApplicationController
  load_and_authorize_resource :team

  before_action :set_event_slot, only: [:show, :edit, :update, :destroy]
  before_action :set_team, only: [:index, :new, :create, :show]

  # GET /event_slots
  # GET /event_slots.json
  def index
    @upcoming_events = @team.event_slots
                           .upcoming
                           .with_product
                           .with_character
                           .order(start_time: :asc)

    @past_events = @team.event_slots
                       .past
                       .with_product
                       .with_character
                       .order(start_time: :asc)
  end

  # GET /event_slots/1
  # GET /event_slots/1.json
  def show
  end

  # GET /event_slots/new
  def new
      @event_slot = @team.event_slots.new
      js :products_search_url => products_search_url
  end

  # GET /event_slots/1/edit
  def edit
    js :products_search_url => products_search_url
  end

  # POST /event_slots
  # POST /event_slots.json
  def create
    @event_slot = @team.event_slots.new(event_slot_params)

    respond_to do |format|
      if @event_slot.save
        format.html { redirect_to [@team, @event_slot], notice: 'Event slot was successfully created.' }
        format.json { render :show, status: :created, location: [@team, @event_slot] }
      else
        format.html { render :new }
        format.json { render json: @event_slot.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /event_slots/1
  # PATCH/PUT /event_slots/1.json
  def update
    respond_to do |format|
      if @event_slot.update(event_slot_params)
        format.html { redirect_to [@team, @event_slot], notice: 'Event slot was successfully updated.' }
        format.json { render :show, status: :ok, location: [@team, @event_slot] }
      else
        format.html { render :edit }
        format.json { render json: @event_slot.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /event_slots/1
  # DELETE /event_slots/1.json
  def destroy
    @event_slot.destroy
    respond_to do |format|
      format.html { redirect_to event_slots_url, notice: 'Event slot was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event_slot
      @event_slot = EventSlot.find(params[:id])
    end

    def set_team
      @team = Team.find(params[:team_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_slot_params
      params.require(:event_slot).permit(:product_id, :character_id, :start_time)
    end
end
