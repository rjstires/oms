class ProductsController < ApplicationController
  def search
    @products = Product.search(params)
  end

  private
    # TODO We need to secure the potential filters.
    def product_params
      params.permit(:filters)
    end
end
