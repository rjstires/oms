json.events @event_slots do |e|
  json.start_time e.start_time
  json.character e.display_character
  json.team do
    json.name e.team.name_alias
    json.faction e.team.faction.name
  end

  json.product do
    json.category e.product.category.name
    json.zone e.product.zone.name
    json.difficulty e.product.difficulty.name

    json.play_style e.product.play_style.name
    json.loot_option e.product.loot_option.name
    json.mount e.product.mount.name
  end
end
