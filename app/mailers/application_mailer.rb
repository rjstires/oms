class ApplicationMailer < ActionMailer::Base
  default(
    from: "BoostingEdge.com <noreply@boostingedge.com>",
    reply_to: "Robert Stires <jarvis.dresden@gmail.com>"
  )
end
